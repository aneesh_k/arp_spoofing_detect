FROM kulkarniane/arp_detect

MAINTAINER Aneesh Kulkarni

LABEL description="ARP spoofing detection script with dependencies"

#RUN mkdir /data

#RUN chmod 777 data

WORKDIR /

#COPY /dump.pcap ./dump.pcap

#RUN cp /home/arp_detect.py /data/

RUN cp /home/arp_detect.py /

ENTRYPOINT ["python", "arp_detect.py"]
