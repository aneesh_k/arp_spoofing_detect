# arp_spoofing

Author: Aneesh Kulkarni

This contains the dockerfile to monitor for APR poisoning and MITM attacks that could potentially arise from the same.
The script monitors attacks using a variety of techniques. 

- There's list which contains MACs of all malicious attackers; a dict which contains mappings of MAC and IP addresses; a dict which contains frequency of ARP requests for a specific IP; a dict with ARP responses for specific MAC addresses; a dict with mapping of MAC and IP of genuine hosts; a dict with mappings of MAC with real IP of potential attackers.
- The script goes through packet capture once to get all the above mentioned data.
- With the data stored after iterating through the capture once, the script verifies the packets again with the knowledge of the mappings and known genuine hosts and malicious attackers. 
- Consequently, the spoofer MAC addresses along with the genuine MAC for a specific IP is printed out.

