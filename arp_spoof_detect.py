import sys
import oyaml as yaml
from collections import OrderedDict
from scapy.all import *


# Parses through all the packets to create data structures as defined above with mappings of potential hostile hosts.
def arp_check():
    all_macs = set()  # Set of all mac to IP mappings for all ARP requests
    duplicate_macs = dict()  # Dict displaying all IPs mapped to each mac in pcap
    current_mac = list()  # Maps current ARP request with required MAC for IP
    arp_req = set()  # Maps all ARP requests
    arp_res = set()  # Maps all ARP responses
    res_frequency = dict()  # Maps frequency of each response for particular MAC
    arp_req_freq = dict()  # Maps frequency of each ARP request for a MAC
    arp_req_dict = dict()  # Maps frequency of each ARP reponse for a MAC
    arp_latest = [None, None]  # Keeps track of latest ARP request
    attackers = list()  # Tracks all attackers
    table = dict()  # Creates a mapping of MAC with IP for non-hostile hosts
    potential_table = dict()  # Creates a mapping of potential hostile hosts with their potential genuine IP Addresses
    i = 0
    j = 0
    del_keys = list()
    out_stream = dict()
    pcap_file = sys.argv[1]
    packets = rdpcap(pcap_file)
    rem_from_table = list()
    for pkt in packets:
        i += 1
        if ARP in pkt:
            # Add all mac and IPs to set.
            all_macs.add((pkt[ARP].hwsrc, pkt[ARP].psrc))
            if pkt[ARP].hwsrc not in duplicate_macs:
                duplicate_macs[pkt[ARP].hwsrc] = list()
                duplicate_macs[pkt[ARP].hwsrc].append(pkt[ARP].psrc)
            elif pkt[ARP].hwsrc in duplicate_macs and pkt[ARP].psrc not in duplicate_macs[pkt[ARP].hwsrc]:
                duplicate_macs[pkt[ARP].hwsrc].append(pkt[ARP].psrc)
        if ARP in pkt and pkt[ARP].op is 1:
                j = i
                arp_req.add((pkt[ARP].pdst , pkt[ARP].psrc))
                arp_latest[0] = pkt[ARP].pdst
                arp_latest[1] = pkt[ARP].psrc
                if pkt[ARP].pdst not in arp_req_dict:
                    arp_req_dict[pkt[ARP].pdst] = pkt[ARP].psrc
                if pkt[ARP].pdst not in arp_req_freq:
                    arp_req_freq[pkt[ARP].pdst] = 1
                elif pkt[ARP].pdst in arp_req_freq:
                    arp_req_freq[pkt[ARP].pdst] += 1
        elif ARP in pkt and pkt[ARP].op is 2:
            arp_res.add((pkt[ARP].hwsrc, pkt[ARP].psrc))
            if pkt[ARP].hwsrc not in res_frequency:
                res_frequency[pkt[ARP].hwsrc] = 1
            elif pkt[ARP].hwsrc in res_frequency:
                res_frequency[pkt[ARP].hwsrc] += 1

        # Check for frequency of requests of host to identify potential attackers, attackers and
        if ARP in pkt and pkt[ARP].op is 2 and i>j and pkt[ARP].psrc in arp_req_dict:
            if res_frequency[pkt[ARP].hwsrc] == arp_req_freq[pkt[ARP].psrc]:
                table[pkt[ARP].hwsrc] = pkt[ARP].psrc
            elif res_frequency[pkt[ARP].hwsrc] > arp_req_freq[pkt[ARP].psrc]:
                if pkt[ARP].hwsrc in potential_table:
                    pass
                elif pkt[ARP].hwsrc not in potential_table:
                    potential_table[pkt[ARP].hwsrc] = pkt[ARP].psrc

            if len(duplicate_macs[pkt[ARP].hwsrc]) > 1:
                if pkt[ARP].hwsrc not in attackers:
                    attackers.append(pkt[ARP].hwsrc)

    for key, value in potential_table.items():
        for key1, value1 in table.items():
            if value1 == value:
                del_keys.append(key)

    for val in del_keys:
        if val in potential_table:
            del potential_table[val]

    for attack in attackers:
        if attack in table:
            rem_from_table.append(attack)
    for rem in rem_from_table:
        del table[rem]

    k = -1
    for p in packets:
        list_yaml = OrderedDict()
        benign_mac = None
        benign_mac2 = None
        k += 1
        if ARP in p and p[ARP].op is 2:
            if p[ARP].hwsrc in attackers and p[ARP].hwsrc not in table:
                if p[ARP].hwsrc in potential_table and p[ARP].psrc == potential_table[p[ARP].hwsrc]:
                    pass
                elif p[ARP].hwsrc in potential_table and p[ARP].psrc != potential_table[p[ARP].hwsrc]:
                    list_yaml["packet_index"] = k
                    list_yaml["victim_mac_address"] = p[ARP].hwdst
                    list_yaml["attacker_mac_address"] = p[ARP].hwsrc
                    list_yaml["spoofed_ipv4_address"] = p[ARP].psrc
                    for key, value in table.items():
                        if value == p[ARP].psrc:
                            benign_mac = key
                    if benign_mac:
                        list_yaml["benign_mac_address"] = benign_mac
                    else:
                        list_yaml["benign_mac_address"] = benign_mac
                    out_stream[k] = list_yaml
                    # print(list_yaml)
                else:
                    list_yaml["packet_index"] = k
                    list_yaml["victim_mac_address"] = p[ARP].hwdst
                    list_yaml["attacker_mac_address"] = p[ARP].hwsrc
                    list_yaml["spoofed_ipv4_address"] = p[ARP].psrc
                    for key, value in table.items():
                        if value == p[ARP].psrc:
                            benign_mac2 = key
                    if benign_mac2:
                        list_yaml["benign_mac_address"] = benign_mac2
                    else:
                        list_yaml["benign_mac_address"] = benign_mac2
                    # print(list_yaml)
                    out_stream[k] = list_yaml

    for place, val_dict in out_stream.items():
        print("---")
        print(yaml.dump(val_dict, default_flow_style=False))


if __name__ == '__main__':
    try:
        sys.exit(arp_check())
    except Exception as e:
        sys.exit(1)
